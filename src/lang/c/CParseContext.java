package lang.c;

import lang.*;

public class CParseContext extends ParseContext {
	private CSymbolTable symboltable;

	public CParseContext(IOContext ioCtx,  CTokenizer tknz) {
		super(ioCtx, tknz);
		this.symboltable = new CSymbolTable();
	}

	@Override
	public CTokenizer getTokenizer()		{ return (CTokenizer) super.getTokenizer(); }
	public CSymbolTable getSymbolTable()	{ return this.symboltable; }
	private int seqNo = 0;
	public int getSeqId() { return ++seqNo; }
}
