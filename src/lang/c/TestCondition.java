package lang.c;

import java.util.ArrayList;

import lang.FatalErrorException;
import lang.IOContext;
import lang.c.parse.Condition;
import lang.c.parse.Declaration;

public class TestCondition {
    private static ArrayList<CParseRule> programs = new ArrayList<CParseRule>();

    public static void main(String[] args) {
        String inFile = args[0]; // 適切なファイルを絶対パスで与えること
        IOContext ioCtx = new IOContext(inFile, System.out, System.err);
        CTokenizer ct = new CTokenizer(new CTokenRule());
        CParseContext pcx = new CParseContext(ioCtx, ct);
        try {
            CToken tk = ct.getNextToken(pcx);
            if(Condition.isFirst(tk)){
                while(Condition.isFirst(tk)) {
                    CParseRule parseTree = new Condition(pcx);
                    parseTree.parse(pcx);
                    programs.add(parseTree);
                    tk = ct.getCurrentToken(pcx);
                }
                tk = ct.getCurrentToken(pcx);
                if (tk.getType() != CToken.TK_EOF) {
                    pcx.fatalError(tk.toExplainString() + "プログラムの最後にゴミがあります");
                }
                if (pcx.hasNoError()){
                    for(CParseRule p : programs){
                        if (p != null) { p.semanticCheck(pcx); }
                    }
                }
                if (pcx.hasNoError()){
                    for(CParseRule p : programs){
                        if (p != null) { p.codeGen(pcx); }
                    }
                }
                pcx.errorReport();
            }else{
                pcx.fatalError(tk.toExplainString() + "プログラムの先頭にゴミがあります");
            }
        } catch (FatalErrorException e) {
            e.printStackTrace();
        }
    }

}
