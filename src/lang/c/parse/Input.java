package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Input extends CParseRule {
    private CToken tk;
    private CParseRule input;
    public Input(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_INPUT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);//inputが取り出される
        tk = ct.getNextToken(pcx);
        if (Primary.isFirst(tk)) {
            input = new Primary(pcx);
            input.parse(pcx);
            tk = ct.getCurrentToken(pcx);//inputが取り出される
        } else {
            pcx.fatalError(tk.toExplainString() + "'input'の後ろはprimaryです");
        }
        if (tk.getType() == CToken.TK_SEMI) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'primary'の後ろは ';' です");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (input != null) {
            input.semanticCheck(pcx);
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        if(input != null){
            PrintStream o = pcx.getIOContext().getOutStream();
            o.println(";;; Input starts");
            input.codeGen(pcx);
            o.println("\tMOV\t#0xFFE0, R0\t; Input:FFE0をR0に入れる");
            o.println("\tMOV\t(R0), R1\t; Input:変数に代入する値をスタックに積む");
            o.println("\tMOV\t-(R6), R0\t; Input: 代入する値取り出し");
            o.println("\tMOV\tR1, (R0)\t; Input: 代入");
            o.println(";;; Input completes");
        }
    }
}
