package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class ConstItem extends CParseRule {
    //private CParseRule
    private boolean isAmp = false;
    private boolean isPointer = false;
    private CToken tk;
    private CType type;
    private CToken ident;
    private CToken value;


    public ConstItem(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_MULT || tk.getType() == CToken.TK_IDENT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {

        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);
        if (tk.getType() == CToken.TK_MULT) {
            isPointer = true;
            tk = ct.getNextToken(pcx);//Identが取り出さる
        }

        if(tk.getType() == CToken.TK_IDENT){
            ident = tk;
            tk = ct.getNextToken(pcx);//Assignが取り出される
        }else{
            pcx.fatalError(tk.toExplainString()+"後ろは識別子です");
        }

        if(tk.getType() == CToken.TK_ASSIGN){
          tk = ct.getNextToken(pcx);//& or NUMが取り出される
        }else{
            pcx.fatalError(tk.toExplainString()+"Identの後ろは'='です");
        }

        if (tk.getType() == CToken.TK_AMPERSAND) {
            tk = ct.getNextToken(pcx);//NUMが取り出さる
            isAmp = true;
        }

        if(tk.getType() == CToken.TK_OCTAL || tk.getType() == CToken.TK_DECIMAL ||tk.getType() == CToken.TK_HEXADECIMAL ){
            value = tk;
            tk = ct.getNextToken(pcx);

        }else{
            pcx.fatalError(tk.toExplainString()+"'=' or ''&の後ろは数字です");
        }

        if(isPointer){
            type = CType.getCType(CType.T_pint);
        }else{
            type = CType.getCType(CType.T_int);
        }

        //記号表へ登録
        CSymbolTable table = pcx.getSymbolTable();
        CSymbolTableEntry entry = new CSymbolTableEntry(type,1,true,true,0);
        if(!table.register(ident.getText(), entry)){
            pcx.fatalError(ident.toExplainString() + "多重定義です");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(type.getType() == CType.T_pint && !isAmp) {
            pcx.fatalError(ident.toExplainString() + "ポインタ変数にアドレス値以外は代入できません");
        }

        if (!isAmp && isPointer) {
            pcx.fatalError(ident.toExplainString() + "左右の変数の型が違います");
        }

        if (isAmp && !isPointer) {
            pcx.fatalError(ident.toExplainString() + "左右の変数の型が違います");
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; ConstItem starts");
        o.println(ident.getText() + "\t=" + value.getText() +"; ConstItem: 定数宣言" + ident.toExplainString());
        o.println(";;; ConstItem completes");
    }
}
