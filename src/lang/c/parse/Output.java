package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Output extends CParseRule {
    private CToken tk;
    private CParseRule output;
    public Output(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_OUTPUT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);//inputが取り出される
        tk = ct.getNextToken(pcx);
        if (Expression.isFirst(tk)) {
            output = new Expression(pcx);
            output.parse(pcx);
            tk = ct.getCurrentToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'output'の後ろはexpressionです");
        }
        if (tk.getType() == CToken.TK_SEMI) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'primary'の後ろは ';' です");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (output != null) {
            output.semanticCheck(pcx);
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        if(output != null){
            PrintStream o = pcx.getIOContext().getOutStream();
            o.println(";;; Output starts");
            output.codeGen(pcx);
            o.println("\tMOV\t#0xFFE0, R0\t; Output:FFE0をR0に入れる");
            o.println("\tMOV\t-(R6), (R0)\t; Output:FFE0に表示する値を入れる");
            o.println(";;; Output completes");
        }
    }
}
