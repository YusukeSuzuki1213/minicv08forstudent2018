package lang.c.parse;

import java.io.PrintStream;
import java.util.ArrayList;

import lang.*;
import lang.c.*;

public class While extends CParseRule {
    private CToken tk;
    private CParseRule condition,statement;
    private ArrayList<CParseRule> statements = new ArrayList<CParseRule>();
    public While(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_WHILE;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);//whileが取り出される
        tk = ct.getNextToken(pcx);//(が取り出される

        if (tk.getType() == CToken.TK_LPAR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'while'の後ろは '(' です");
        }

        if (Condition.isFirst(tk)) {
            condition = new Condition(pcx);
            condition.parse(pcx);
            tk = ct.getCurrentToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'('の後ろはconditionです");
        }

        if (tk.getType() == CToken.TK_RPAR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'condition'の後ろは ')' です");
        }

        if (tk.getType() == CToken.TK_LCUR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "')'の後ろは '{' です");
        }

        while (Statement.isFirst(tk)) {
            statement = new Statement(pcx);
            statement.parse(pcx);
            statements.add(statement);
            tk = ct.getCurrentToken(pcx);
        }
        if (tk.getType() == CToken.TK_RCUR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'statement or {'の後ろは '}' です");
        }


    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (condition != null){
            condition.semanticCheck(pcx);
        }
        for(CParseRule s : statements){
            if (s != null) { s.semanticCheck(pcx); }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; While starts");
        if(condition != null){
            int seq = pcx.getSeqId();
            o.println("WHILESTART" + seq + ":");
            condition.codeGen(pcx);
            o.println("\tMOV\t-(R6), R0\t; While:真偽値を取り出す");
            o.println("\tBRZ\tWHILEEND" + seq + "\t; While:偽のとき、用意したラベルに飛ぶ");
            for(CParseRule s : statements){
                if (s != null) { s.codeGen(pcx); }
            }
            o.println("\tJMP\tWHILESTART" + seq + "\t; While:whileの最初に飛ぶ");
            o.println("WHILEEND" + seq + ":");
        }
        o.println(";;; While completes");
    }

}

