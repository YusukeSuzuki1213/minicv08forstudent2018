package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;


public class StatementAssign extends CParseRule {
    private CToken tk,tk_ass,nntk, tk_semi;
    private CParseRule primary, expression;

    public StatementAssign(CParseContext pcx){

    }
    public static boolean isFirst(CToken tk) {
        return Primary.isFirst(tk);
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);
        primary= new Primary(pcx);
        primary.parse(pcx);

        tk_ass = ct.getCurrentToken(pcx);
        if ( tk_ass.getType() == CToken.TK_ASSIGN ) {
            nntk = ct.getNextToken(pcx);
            if (Expression.isFirst(nntk)) {
                expression= new Expression(pcx);
                expression.parse(pcx);
            } else {
                pcx.fatalError(nntk.toExplainString() + "'='の後ろはexpressionです");
            }
            tk_semi = ct.getCurrentToken(pcx);
            if ( tk_semi.getType() == CToken.TK_SEMI ) {
                ct.getNextToken(pcx);
            } else {
                pcx.fatalError(tk_semi.toExplainString() + "expressinの後ろは ';' です");
            }
        } else {
            pcx.fatalError(tk_ass.toExplainString() + "primaryの後ろは'='です。");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        int lt = 0, rt = 0;
        boolean lc = false;
        if ( primary != null ) {
            primary.semanticCheck(pcx);
            lt = primary.getCType().getType();		// =の左辺の型
            lc = primary.isConstant();
        } else {
            pcx.fatalError(tk_ass.toExplainString() + "左辺がありません");
        }
        if ( expression != null ){
            expression.semanticCheck(pcx);
            rt = expression.getCType().getType();		// =の左辺の型
        } else {
            pcx.fatalError(tk_ass.toExplainString() + "右辺がありません");
        }
        if (lt != rt) {
            pcx.fatalError(tk_ass.toExplainString() + "左辺の型[" + primary.getCType().toString() + "]と右辺の型[" + expression.getCType().toString() + "]の型が違います。");
        } else if(lc){//左辺が定数だったら
            pcx.fatalError(tk_ass.toExplainString() + "左辺が定数であるため、代入不可能です。");
        }

    }
    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; StatementAssign starts");
        if( primary != null && expression != null ){
            primary.codeGen(pcx);
            expression.codeGen(pcx);
            o.println("\tMOV\t-(R6), R0\t; StatementAssign:;");   //expressionを取り出す。
            o.println("\tMOV\t-(R6), R1\t; StatementAssign:;");   //primaryを取り出す。
            o.println("\tMOV\t  R0, (R1)\t; StatementAssign:;"); //
        }
        o.println(";;; StatementAssign completes");
    }
}
