package lang.c.parse;

import java.io.PrintStream;
import java.util.ArrayList;

import lang.*;
import lang.c.*;

public class If extends CParseRule {
    private CToken lbra,tk;
    private CParseRule condition, statement;
    private ArrayList<CParseRule> statements_if = new ArrayList<CParseRule>();
    private ArrayList<CParseRule> statements_else = new ArrayList<CParseRule>();
    public If(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_IF;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);// 'if'が取り出される
        tk = ct.getNextToken(pcx);// '('が取り出さる

        if (tk.getType() == CToken.TK_LPAR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "ifの後ろは '(' です");
        }
        if (Condition.isFirst(tk)) {
            condition = new Condition(pcx);
            condition.parse(pcx);
            tk = ct.getCurrentToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'condition'の後ろは')'です");
        }

        if (tk.getType() == CToken.TK_RPAR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'LPAR or condition'の後ろは ')' です");
        }

        if (tk.getType() == CToken.TK_LCUR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "')'の後ろは '{' です");
        }

        while (Statement.isFirst(tk)) {
            statement = new Statement(pcx);
            statement.parse(pcx);
            statements_if.add(statement);
            tk = ct.getCurrentToken(pcx);
        }

        if (tk.getType() == CToken.TK_RCUR) {
            tk = ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "'statement or LCUR'の後ろは '}' です");
        }

        if (tk.getType() == CToken.TK_ELSE) {
            tk = ct.getNextToken(pcx);
            if (tk.getType() == CToken.TK_LCUR) {
                tk = ct.getNextToken(pcx);
            } else {
                pcx.fatalError(tk.toExplainString() + "'else'の後ろは '{' です");
            }
            while (Statement.isFirst(tk)) {
                statement = new Statement(pcx);
                statement.parse(pcx);
                statements_else.add(statement);
                tk = ct.getCurrentToken(pcx);
            }
            if (tk.getType() == CToken.TK_RCUR) {
                tk = ct.getNextToken(pcx);
            } else {
                pcx.fatalError(tk.toExplainString() + "'statement or LCUR'の後ろは '}' です");
            }
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (condition != null){
            condition.semanticCheck(pcx);
        }
        for(CParseRule s : statements_if){
            if (s != null) { s.semanticCheck(pcx); }
        }
        for(CParseRule s : statements_else){
            if (s != null) { s.semanticCheck(pcx); }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; If starts");
        if(condition != null){
            condition.codeGen(pcx);
            int seq = pcx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; If:真偽値を取り出す");
            o.println("\tBRZ\tELSE" + seq + "\t\t; If:偽のとき、用意したラベルに飛ぶ");
            for(CParseRule si : statements_if){
                if (si != null) { si.codeGen(pcx); }
            }
            o.println("\tJMP\tENDIF" + seq + "\t\t; If:ifの終わりに飛ぶ");
            o.println("ELSE" + seq + ":");
            for(CParseRule se : statements_else){
                if (se != null) { se.codeGen(pcx); }
            }
            o.println("ENDIF"+ seq + ":");
        }
        o.println(";;; If completes");

    }
}
